﻿using System;
using DataAccess.DataModels;

namespace DataAccess.Interfaces
{
	public interface IVisitRepository : IDisposable, IRepository<VisitDto>
	{

	}
}

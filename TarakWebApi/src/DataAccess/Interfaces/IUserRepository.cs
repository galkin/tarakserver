﻿using System;
using DataAccess.DataModels;

namespace DataAccess.Interfaces
{
	public interface IUserRepository : IDisposable, IRepository<UserDto>
	{

	}
}

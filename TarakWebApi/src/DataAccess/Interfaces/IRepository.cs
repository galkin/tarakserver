﻿using System.Collections.Generic;

namespace DataAccess.Interfaces
{
	public interface IRepository<T>
	{
		T Add(T item);
		void Delete(T item);
		T Update(T item);
		T FindById(int id);
		IEnumerable<T> FindAll();
	}
}

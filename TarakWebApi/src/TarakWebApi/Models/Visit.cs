﻿using System;

namespace TarakWebApi.Models
{
	public class Visit
	{
		public int Id { get; set; }
		public string Description { get; set; }
		public DateTime CheckInDate { get; set; }
		public DateTime CheckOutDate { get; set; }
		public string TimeZoneInfo { get; set; }
		public string VisitorName { get; set; }
		public int UserId { get; set; }
	}
}

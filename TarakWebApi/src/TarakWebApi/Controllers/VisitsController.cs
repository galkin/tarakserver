﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DataAccess.DataModels;
using DataAccess.Interfaces;
using Microsoft.AspNetCore.Mvc;
using TarakWebApi.Models;

namespace TarakWebApi.Controllers
{
    [Route("api/[controller]")]
    public class VisitsController : Controller
    {
        
        public IVisitRepository VisitRepository { get; set; }

        public VisitsController(IVisitRepository visitRepository)
        {
            VisitRepository = visitRepository;
        }

        // GET api/v1/Visits
        [HttpGet]
        public IEnumerable<Visit> Get()
	    {
		    return
			    VisitRepository.
				    FindAll().
				    Select(v => new Visit
				    {
					    Id = v.Id,
					    Description  = v.Description,
					    CheckInDate  = v.CheckInDate,
					    CheckOutDate = v.CheckOutDate,
						TimeZoneInfo = v.TimeZoneInfo,
						VisitorName  = v.VisitorName,
						UserId = v.UserId
				    });
	    }

        // GET api/v1/Visits/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var v = VisitRepository.FindById(id);
            var visit = new Visit
            {
                Id = v.Id,
                Description = v.Description,
                CheckInDate = v.CheckInDate,
                CheckOutDate = v.CheckOutDate,
                TimeZoneInfo = v.TimeZoneInfo,
                VisitorName = v.VisitorName,
                UserId = v.UserId
            };

            return new ObjectResult(visit);
        }

        // POST: api/v1/Visits
        [HttpPost]
        public IActionResult Post([FromBody] Visit visitData)
        {
            if (visitData == null)
                return BadRequest();

            var identity = (ClaimsIdentity) User.Identity;
            var userId = Convert.ToInt32(identity.FindFirst(ClaimTypes.Sid).Value);

            var visit = new VisitDto
            {

                TimeZoneInfo = visitData.TimeZoneInfo,
                VisitorName = visitData.VisitorName,
                CheckInDate = visitData.CheckInDate,
                CheckOutDate = visitData.CheckOutDate,
                Id = visitData.Id,
                Description = visitData.Description,
                UserId = userId
            };

            VisitRepository.Add(visit);
            return Ok();
        }

        // PUT: api/v1/Visits/5
        [HttpPut]
        public IActionResult Put([FromBody] Visit visitData)
        {

            var identity = (ClaimsIdentity) User.Identity;
            var userId = Convert.ToInt32(identity.FindFirst(ClaimTypes.Sid).Value);

            var v = new VisitDto
            {
                Id = visitData.Id,
                Description = visitData.Description,
                CheckInDate = visitData.CheckInDate,
                CheckOutDate = visitData.CheckOutDate,
                UserId = userId
            };

            VisitRepository.Update(v);
            return Ok();
        }

        // DELETE: api/v1/Visits/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var v = new VisitDto
            {
                Id = id,
            };

            VisitRepository.Delete(v);
            return new NoContentResult();
        }
    }
}

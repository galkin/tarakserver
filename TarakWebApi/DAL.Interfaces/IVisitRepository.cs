﻿using System;
using DataAccess.Interfaces;

namespace DAL.Interfaces
{
	public interface IVisitRepository : IDisposable, IRepository<VisitDto>
	{

	}
}

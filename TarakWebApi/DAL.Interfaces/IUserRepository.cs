﻿using System;
using DataAccess.Model;

namespace DataAccess.Interfaces
{
	public interface IUserRepository : IDisposable, IRepository<UserDto>
	{

	}
}

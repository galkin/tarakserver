﻿using System;
using DAL.DataModels;

namespace DAL.Interfaces
{
	public interface IVisitRepository : IDisposable, IRepository<VisitDto>
	{

	}
}

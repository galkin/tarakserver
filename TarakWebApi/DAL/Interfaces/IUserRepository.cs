﻿using System;
using DAL.DataModels;

namespace DAL.Interfaces
{
	public interface IUserRepository : IDisposable, IRepository<UserDto>
	{

	}
}

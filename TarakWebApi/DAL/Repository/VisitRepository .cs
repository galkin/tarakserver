﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using DAL.DataModels;
using DAL.Interfaces;

namespace DAL.Repository
{
	public class VisitRepository : IVisitRepository, IDisposable
	{

		private readonly IDbConnection _db;

		public VisitRepository()
		{
			_db = new SqlConnection(ConfigProvider.DbConnectionString() );
		}
		 
		public IEnumerable<VisitDto> FindAll()
        {
			var list = _db.Query<VisitDto>(@"SELECT [id]
											  ,[Description]
											  ,[CheckInDate]
											  ,[CheckOutDate]
											  ,[TimeZoneInfo]
                                              ,[VisitorName]   
                                              ,[UserId]
										  FROM Visits").ToList();
			return list;
        }

		public VisitDto FindById(int id)
        {
			string query = $@"SELECT [id]
											  ,[Description]
											  ,[CheckInDate]
											  ,[CheckOutDate]
											  ,[TimeZoneInfo]
											  ,[VisitorName] 
                                              ,[UserId]
                                          FROM Visits
                                          WHERE Id = {id}";
			return _db.Query<VisitDto>(query).FirstOrDefault();
        }

		public VisitDto Add(VisitDto visit)
        {
			const string sqlQuery = @"INSERT INTO Visits ([Description], [CheckInDate], [CheckOutDate], [TimeZoneInfo], [VisitorName], [UserId]  ) 
                                      VALUES(@Description, @CheckInDate, @CheckOutDate, @TimeZoneInfo, @VisitorName, @UserId); 
                                      SELECT CAST(SCOPE_IDENTITY() as INT)";
			visit.Id = _db.Query<int>(sqlQuery, visit).Single();
			return visit;
        }

		public void Delete(VisitDto visit)
		{
			var sqlQuery = $@"DELETE FROM Visits WHERE Id = {visit.Id}";
			_db.Execute(sqlQuery);
		}

		public VisitDto Update(VisitDto visit)
		{
			var sqlQuery = $@"UPDATE Visits 
                                           SET 
                                              Description  =   '{visit.Description}',
                                              CheckInDate  =   '{visit.CheckInDate}', 
                                              CheckOutDate =   '{visit.CheckOutDate}', 
                                              TimeZoneInfo =   '{visit.TimeZoneInfo}',
                                              VisitorName  =   '{visit.VisitorName}',
                                              UserId = '{visit.UserId}'
                                           WHERE Id = {visit.Id}";
			
			_db.Execute(sqlQuery, visit);
			return visit;
		}

        public void Dispose()
        {
             GC.SuppressFinalize(this);
        }
	}
}

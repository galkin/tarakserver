﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Web.Http;
using DataAccess.Interfaces;
using DataAccess.Model;
using DataAccess.Repository;
using TarakServer.Filters;
using TarakServer.Models;

namespace TarakServer.Controllers.V1
{
	[TarakAuthorize]
	public class VisitsController : ApiController
    {

	  private readonly IVisitRepository _visitRepository;

      public VisitsController()
      {
		  _visitRepository = new VisitRepository();
      }

	  public VisitsController(IVisitRepository visitRepository)
      {
		  _visitRepository = visitRepository;
      }

		 // GET api/v1/Visits
	    public IEnumerable<Visit> Get()
	    {
		    return
			    _visitRepository.
				    FindAll().
				    Select(v => new Visit
				    {
					    Id = v.Id,
					    Description  = v.Description,
					    CheckInDate  = v.CheckInDate,
					    CheckOutDate = v.CheckOutDate,
						TimeZoneInfo = v.TimeZoneInfo,
						VisitorName  = v.VisitorName,
						UserId = v.UserId
				    });
	    }

		// GET api/v1/Visits/5
	  public Visit Get(int id)
		{
			var v = _visitRepository.FindById(id);
			var visit = new Visit
			{
				Id = v.Id,
				Description = v.Description,
				CheckInDate = v.CheckInDate,
				CheckOutDate = v.CheckOutDate,
				TimeZoneInfo = v.TimeZoneInfo,
				VisitorName = v.VisitorName,
				UserId = v.UserId
			};

			return visit;
		}

		// POST: api/v1/Visits
		[HttpPost]
		public HttpResponseMessage Post([FromBody]Visit visitData)
	    {
		    try
		    {
				if (visitData == null) return Request.CreateResponse(HttpStatusCode.InternalServerError, "Invalid content");

				var identity = (ClaimsIdentity)User.Identity;
				var userId = Convert.ToInt32(identity.FindFirst(ClaimTypes.Sid).Value);

				var visit = new VisitDto
				{
					
					TimeZoneInfo = visitData.TimeZoneInfo,
					VisitorName = visitData.VisitorName,
					CheckInDate = visitData.CheckInDate,
					CheckOutDate = visitData.CheckOutDate,
					Id = visitData.Id,
					Description = visitData.Description,
					UserId = userId
				};

			    _visitRepository.Add(visit);
			    return Request.CreateResponse(HttpStatusCode.OK);
		    }
		    catch (Exception ex)
		    {
			    return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
		    }
	    }

		// PUT: api/v1/Visits/5
		[HttpPut]
		public HttpResponseMessage Put([FromBody]Visit visitData)
		{
			try
			{
				var identity = (ClaimsIdentity)User.Identity;
				var userId = Convert.ToInt32(identity.FindFirst(ClaimTypes.Sid).Value);

				var v = new VisitDto
				{
					Id = visitData.Id,
					Description = visitData.Description,
					CheckInDate = visitData.CheckInDate,
					CheckOutDate = visitData.CheckOutDate,
					UserId = userId
				};

				_visitRepository.Update(v);

				var response = Request.CreateResponse(HttpStatusCode.OK, v);
				response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
				return response;
			}
			catch (Exception ex)
			{
				return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		// DELETE: api/v1/Visits/5
		public HttpResponseMessage Delete(int id)
		{
			try
			{
				var v = new VisitDto
				{
					Id = id,
				};

				_visitRepository.Delete(v);
				return Request.CreateResponse(HttpStatusCode.OK);
			}
			catch (Exception ex)
			{
				return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using DataAccess.Interfaces;
using DataAccess.Model;
using DataAccess.Repository;
using TarakServer.Models;

namespace TarakServer.Controllers
{
	[Authorize]
	public class UsersController : ApiController
    {

		 private readonly IUserRepository _userRepository;

      public UsersController()
      {
		  _userRepository = new UserRepository();
      }

	  public UsersController(IUserRepository userRepository)
      {
		  _userRepository = userRepository;
      }

		// GET api/v1/Users
		public IEnumerable<TarakUser> Get()
	    {
		    return
			    _userRepository.
				    FindAll().
				    Select(v => new TarakUser
					{
					    Id = v.Id,
						PhoneNumber = v.PhoneNumber,
						Email =        v.Email,
						FirstName =    v.FirstName,
						LastName =     v.LastName
				    });
	    }

		// POST: api/v1/Users
		[HttpPost]
		public HttpResponseMessage Post([FromBody]TarakUser userData)
	    {
		    try
		    {
			    if (userData == null) return Request.CreateResponse(HttpStatusCode.InternalServerError, "Invalid content");

				var item = new UserDto
				{
					Id = (long) userData.Id,
					PhoneNumber = userData.PhoneNumber,
					Email = userData.Email,
					FirstName = userData.FirstName,
					LastName = userData.LastName,
				};

			    _userRepository.Add(item);
			    return Request.CreateResponse(HttpStatusCode.OK);
		    }
		    catch (Exception ex)
		    {
			    return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
		    }
	    }

		// PUT: api/v1/Users/5
		[HttpPut]
		public HttpResponseMessage Put([FromBody]TarakUser userData)
		{
			try
			{
				var userDto = new UserDto
				{
					Id = userData.Id,
					PhoneNumber = userData.PhoneNumber,
					Email = userData.Email,
					FirstName = userData.FirstName,
					LastName = userData.LastName
				};

				_userRepository.Update(userDto);

				var response = Request.CreateResponse(HttpStatusCode.OK, userDto);
				response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
				return response;
			}
			catch (Exception ex)
			{
				return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		// DELETE: api/v1/Users/5
		public HttpResponseMessage Delete(int id)
		{
			try
			{
				var u = new UserDto
				{
					Id = id,
				};

				_userRepository.Delete(u);
				return Request.CreateResponse(HttpStatusCode.OK);
			}
			catch (Exception ex)
			{
				return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}
    }
}

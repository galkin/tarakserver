﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using DataAccess.Interfaces;
using DataAccess.Model;
using DataAccess.Repository;
using TarakServer.Filters;
using TarakServer.Models;

namespace TarakServer.Controllers.V1
{
	[TarakAuthorize]
	public class TodayVisitsController : ApiController
	{
		private readonly IVisitRepository _visitRepository;

		public TodayVisitsController()
		{
			_visitRepository = new VisitRepository();
		}

		public TodayVisitsController(IVisitRepository visitRepository)
		{
			_visitRepository = visitRepository;
		}

		[Route(@"api/v1/TodayVisits/{today:datetime}")]
		[HttpGet]
		public IEnumerable<Visit> GetTodayVisits(DateTime today)
		{
			return _visitRepository.FindAll().Select(CreateVisit).Where(i => i.CheckInDate.Date == today.Date);
		}

		[Route(@"api/v1/YearlyVisits/{year:int}")]
		[HttpGet]
		public IEnumerable<Visit> GetYearlyVisits(int year)
		{
			var identity = (ClaimsIdentity)User.Identity;
			var userId = Convert.ToInt32(identity.FindFirst(ClaimTypes.Sid).Value);
			return _visitRepository.FindAll() .Select(CreateVisit). Where(i => i.CheckInDate.Date.Year == year && i.UserId == userId);
		}

		private static Visit CreateVisit(VisitDto v)
		{
			return new Visit
			{
				Id = v.Id,
				Description = v.Description,
				CheckInDate = v.CheckInDate,
				CheckOutDate = v.CheckOutDate,
				TimeZoneInfo = v.TimeZoneInfo,
				VisitorName = v.VisitorName,
				UserId = v.UserId
			};
		}
	}
}

﻿using System;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace TarakServer.Models
{
	public class TarakUserRole : IdentityUserRole<long> { }
	public class TarakUserClaim : IdentityUserClaim<long> { }
	public class TarakUserLogin : IdentityUserLogin<long> { }

	public class TarakRole : IdentityRole<long, TarakUserRole>
	{
		public TarakRole() { }
		public TarakRole(string name) { Name = name; }
	}

	public class TarakUserStore : UserStore<TarakUser, TarakRole, long, TarakUserLogin, TarakUserRole, TarakUserClaim>
	{
		public TarakUserStore(ApplicationDbContext context) : base(context)
		{
		}
	}

	public class TarakRoleStore : RoleStore<TarakRole, long, TarakUserRole>
	{
		public TarakRoleStore(ApplicationDbContext context) : base(context)
		{
		}
	}

	// You can add profile data for the user by adding more properties to your TarakUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
	public class TarakUser : IdentityUser<long, TarakUserLogin, TarakUserRole, TarakUserClaim>
	{
		public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<TarakUser, long> manager, string authenticationType)
		{
			// Note the authenticationType must match the one defined in
			// CookieAuthenticationOptions.authenticationType 

			var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
			// Add custom user claims here 
			return userIdentity;
		}

		public string FirstName { get; set; }
		public string LastName { get; set; }
		//public DateTime CreationDate { get; set; }
	}

	public class ApplicationDbContext : IdentityDbContext<TarakUser, TarakRole, long, TarakUserLogin, TarakUserRole, TarakUserClaim>
	{
        public ApplicationDbContext() : base("DBConnectionStringServer") { }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder); // This needs to go before the other rules!

			modelBuilder.Entity<TarakUser>().ToTable("Users");
			modelBuilder.Entity<TarakRole>().ToTable("Roles");
			modelBuilder.Entity<TarakUserRole>().ToTable("UserRoles");
			modelBuilder.Entity<TarakUserClaim>().ToTable("UserClaims");
			modelBuilder.Entity<TarakUserLogin>().ToTable("UserLogins");
		}

		public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}
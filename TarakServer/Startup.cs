﻿using System.Web.Http;
using Microsoft.Owin;
using Owin;
using TarakServer;

[assembly: OwinStartup(typeof(Startup))]

namespace TarakServer
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
			//ConfigureAuth(app);
			HttpConfiguration config = new HttpConfiguration();

			ConfigureAuth(app);

			WebApiConfig.Register(config);
			app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
			app.UseWebApi(config);
		}
    }
}

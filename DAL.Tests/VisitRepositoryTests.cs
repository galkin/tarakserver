﻿using System;
using DataAccess.Interfaces;
using DataAccess.Model;
using DataAccess.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DAL.Tests
{
	[TestClass]
	public class VisitRepositoryTests
	{
		private IVisitRepository _visitRepository;

		[TestInitialize]
		public void Setup()
		{
			_visitRepository = new VisitRepository();
		}

		[TestMethod]
		public void Update()
		{
			var visitId = 119;
			var v = new VisitDto
			{
				Id = visitId,
				Description = "New updated desc",
				CheckInDate = DateTime.Now,
				CheckOutDate = DateTime.Now.AddHours(1)
			};

			_visitRepository.Update(v);
			var visit = _visitRepository.FindById(visitId);
			Assert.AreEqual(visitId, visit.Id);
		}

		[TestMethod]
		public void Add()
		{
			var v = new VisitDto
			{
				Id = 0,
				Description = "New updated desc",
				CheckInDate = DateTime.Now,
				CheckOutDate = DateTime.Now.AddHours(1),
				UserId = 1
			};

			var newVisit =  _visitRepository.Add(v);
			var visit = _visitRepository.FindById(newVisit.Id);
			Assert.AreNotEqual(0, visit.Id);
		}
	}
}

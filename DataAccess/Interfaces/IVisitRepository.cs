﻿using System;
using System.Collections.Generic;
using DataAccess.Model;

namespace DataAccess.Interfaces
{
	public interface IVisitRepository : IDisposable, IRepository<VisitDto>
	{

	}
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using DataAccess.Interfaces;
using DataAccess.Model;

namespace DataAccess.Repository
{
	public class VisitRepository : IVisitRepository, IDisposable
	{

		private readonly IDbConnection _db;

		public VisitRepository()
		{
			_db = new SqlConnection(ConfigProvider.DbConnectionString() );
		}
		 
		public IEnumerable<VisitDto> FindAll()
        {
			var list = _db.Query<VisitDto>(@"SELECT [id]
											  ,[Description]
											  ,[CheckInDate]
											  ,[CheckOutDate]
											  ,[TimeZoneInfo]
                                              ,[VisitorName]   
                                              ,[UserId]
										  FROM Visits").ToList();
			return list;
        }

		public VisitDto FindById(int id)
        {
			string query = string.Format(@"SELECT [id]
											  ,[Description]
											  ,[CheckInDate]
											  ,[CheckOutDate]
											  ,[TimeZoneInfo]
											  ,[VisitorName] 
                                              ,[UserId]
                                          FROM Visits
                                          WHERE Id = {0}", id);
			return _db.Query<VisitDto>(query).FirstOrDefault();
        }

		public VisitDto Add(VisitDto visit)
        {
			const string sqlQuery = @"INSERT INTO Visits ([Description], [CheckInDate], [CheckOutDate], [TimeZoneInfo], [VisitorName], [UserId]  ) 
                                      VALUES(@Description, @CheckInDate, @CheckOutDate, @TimeZoneInfo, @VisitorName, @UserId); 
                                      SELECT CAST(SCOPE_IDENTITY() as INT)";
			visit.Id = _db.Query<int>(sqlQuery, visit).Single();
			return visit;
        }

		public void Delete(VisitDto visit)
		{
			var sqlQuery = string.Format(@"DELETE FROM Visits WHERE Id = {0}", visit.Id);
			_db.Execute(sqlQuery);
		}

		public VisitDto Update(VisitDto visit)
		{
			var sqlQuery = string.Format(@"UPDATE Visits 
                                           SET 
                                              Description  =   '{0}',
                                              CheckInDate  =   '{1}', 
                                              CheckOutDate =   '{2}', 
                                              TimeZoneInfo =   '{3}',
                                              VisitorName  =   '{4}',
                                              UserId = '{5}'
                                           WHERE Id = {6}", 
										   visit.Description, 
										   visit.CheckInDate,
										   visit.CheckOutDate,
										   visit.TimeZoneInfo,
										   visit.VisitorName,
										   visit.UserId,
										   visit.Id);
			
			_db.Execute(sqlQuery, visit);
			return visit;
		}

        public void Dispose()
        {
             GC.SuppressFinalize(this);
        }
	}
}

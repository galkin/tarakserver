﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using DataAccess.Interfaces;
using DataAccess.Model;

namespace DataAccess.Repository
{
	public class UserRepository : IUserRepository, IDisposable
	{
		private readonly IDbConnection _db;

		public UserRepository()
		{
			_db = new SqlConnection(ConfigProvider.DbConnectionString());
		}

		public UserDto Add(UserDto item)
		{
			const string sqlQuery = @"INSERT INTO Users ([FirstName], [LastName], [Email], [Password], [PhoneNumber] ) 
                                      VALUES(@FirstName, @LastName, @Email, @Password, @PhoneNumber); 
                                      SELECT CAST(SCOPE_IDENTITY() as INT)";
			item.Id = _db.Query<int>(sqlQuery, item).Single();
			return item;
		}

		public void Delete(UserDto item)
		{
			throw new NotImplementedException();
		}

		public UserDto Update(UserDto item)
		{
			throw new NotImplementedException();
		}

		public UserDto FindById(int id)
		{
			string query = $@"SELECT 
                                            [Id], 
                                            [FirstName], 
                                            [LastName], 
                                            [Email], 
                                            [Password], 
                                            [PhoneNumber] 
                                          FROM Visits
                                          WHERE Id = {id}";

			return _db.Query<UserDto>(query).FirstOrDefault();
		}

		public IEnumerable<UserDto> FindAll()
		{
			var list = _db.Query<UserDto>(@"SELECT [Id], 
                                            [FirstName], 
                                            [LastName], 
                                            [Email], 
                                            [Password], 
                                            [PhoneNumber]
										  FROM Users").ToList();
			return list;
		}

		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
	}
}

﻿using System.Configuration;

namespace DataAccess
{
	public static class ConfigProvider
	{
		public static string DbConnectionString()
		{
			string connectionString = ConfigurationManager.ConnectionStrings["DBConnectionStringServer"].ConnectionString;

			return connectionString;
		}
	}
}